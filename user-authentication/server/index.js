var express = require('express')
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoClient = require('mongodb').MongoClient;

var app = express();
app.use(bodyParser());
app.use(cors());

app.post('/users', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/UserDB', function(err, db){
        if (!err) {
            var user = req.body;
            db.collection('users').insertOne(user, function(err, response){
                if (!err) {
                    resp.send('User added successfully.');
                } else {
                    resp.send('Error adding user');
                }
            });
        }
    });
});

app.get('/users/:username/:password', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/UserDB', function(err, db){
        if (!err) {
            var username = req.params.username;
            var password = req.params.password;
            db.collection('users').findOne({ username: username, password: password }, function(err, response){
                if (!err && response != null) {
                    resp.send({'message': 'User located' });
                } else {
                    resp.send({'message': 'User not located' });
                }
            });
        }
    });
});