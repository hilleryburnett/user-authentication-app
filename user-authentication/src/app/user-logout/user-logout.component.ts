import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-logout',
  templateUrl: './user-logout.component.html',
  styleUrls: ['./user-logout.component.css']
})
export class UserLogoutComponent implements OnInit {

  message: string = '';
  constructor() {
    if (localStorage.getItem('username')!= null){
      this.message = `Hey ${localStorage.getItem("username")}, you are logged out`
      localStorage.removeItem('username');
    }
   }

  ngOnInit() {
  }

}
