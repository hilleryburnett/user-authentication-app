import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  username: string;
  password: string;
  url:string = "http://localhost:9000/users";
  failureMessage: string = "";
  constructor( private _http: HttpClient) { }

  ngOnInit() {
  }

  ValidateLoginCredentials(){
    var url = `${this.url}/${this.username}/${this.password}`;
    this._http.get<{ 'message': string}>(url).subscribe(x => {
      console.log(x);
      if (x.message == 'User Found'){
        console.log('Login Successful');
        localStorage.setItem('username', this.username);
      } else {
        console.log("Login is not Successful");
        this.failureMessage = 'invalid User Credentials'
      }
    });
  }
}
